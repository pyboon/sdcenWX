<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

/**
 * 系统配文件
 * 所有系统级别的配置
 */
return array(
    /* 模块相关配置 */
    'AUTOLOAD_NAMESPACE' => array('Addons' => ONETHINK_ADDON_PATH), //扩展模块列表
    'DEFAULT_MODULE'     => 'Home',
    'MODULE_DENY_LIST'   => array('Common','User','Admin','Install'),
    //'MODULE_ALLOW_LIST'  => array('Home','Admin'),

    /* 系统数据加密设置 */
    'DATA_AUTH_KEY' => 'CV~_ZJ7aURDkPg|eu*/{Szt34L=^b@[!`m-+?hIF', //默认数据加密KEY
    //油智汇微信公众号信息
    /* 'WXAPPID' => 'wx7cd54c96d649bf01',
    'WXAPPSECRET' => '86706fd6a91099feb6a95e1a1e9c93e6',
     */
    ///和智学堂微信号信息
    'Academic_WXAPPID' => 'wx0a2d6eba48b7f633',///和智学堂
    'Academic_WXAPPSECRET' => '12bd2642b3042327fee7ca81566290a9',
    'Academic_EncodingAESKey' => 'J6nkkLcw8CH3FVz5SiB88yEcBnQPwOHRqlxyHJXxXL5',
    'Academic_TOKEN' => 'zhaoming',
    
    ///和智草堂微信号信息
    'Non_Academic_WXAPPID' => 'wx5d61b3b05c53970d',///和智学堂
    'Non_Academic_WXAPPSECRET' => 'da637b78bfbf394633b0aec47ff5b019',
    'Non_Academic_EncodingAESKey' => 'SshAivSTIunTrPGzLhxkXGfTJyQ10focLeMxay7fWad',
    'Non_Academic_TOKEN' => 'zhaoming',
    /* 用户相关设置 */
    'USER_MAX_CACHE'     => 1000, //最大缓存用户数
    'USER_ADMINISTRATOR' => 1, //管理员用户ID

    /* URL配置 */
    'URL_CASE_INSENSITIVE' => true, //默认false 表示URL区分大小写 true则表示不区分大小写
    'URL_MODEL'            => 1, //URL模式
    'VAR_URL_PARAMS'       => '', // PATHINFO URL参数变量
    'URL_PATHINFO_DEPR'    => '/', //PATHINFO URL分割符
    'URL_HTML_SUFFIX'=>'',
    /* 全局过滤配置 */
    'DEFAULT_FILTER' => '', //全局过滤函数

    /* 数据库配置 */
    'DB_TYPE'   => 'mysql', // 数据库类型
    'DB_HOST'   => '127.0.0.1', // 服务器地址
    'DB_NAME'   => 'sdcenwx', // 数据库名
    'DB_USER'   => 'root', // 用户名
    'DB_PWD'    => '',  // 密码
    'DB_PORT'   => '3306', // 端口
    'DB_PREFIX' => 'sdcenwx_', // 数据库表前缀

    /* 文档模型配置 (文档模型核心配置，请勿更改) */
    'DOCUMENT_MODEL_TYPE' => array(2 => '主题', 1 => '目录', 3 => '段落'),
    
    //SQLSERVER 数据库连接配置
    'DB_SQLSERVER_232_CONFIG' => array(
        'db_type'  => 'sqlsrv',
        'db_user'  => 'sa',
        'db_pwd'   => '1qazWSX',
        'db_host'  => '121.251.253.232',
        'db_port'  => '1433',
        'db_name'  => 'OpenAuthDB'
    ),
    'DB_SQLSERVER_FMS_TEST_CONFIG' => array(  
        'db_type'  => 'sqlsrv',
        'db_user'  => 'sa',
        'db_pwd'   => '1qaz@WSX',
        'db_host'  => '121.251.253.230',
        'db_port'  => '1433',
        'db_name'  => 'FMS'
    ),
    'DB_SQLSERVER_FMS_NEW_CONFIG' => array(
        'db_type'  => 'sqlsrv',
        'db_user'  => 'sa',
        'db_pwd'   => '1qaz@WSX',
        'db_host'  => '121.251.253.229',
        'db_port'  => '1433',
        'db_name'  => 'FMS_New'
    ),
    'DB_SQLSERVER_TMSSD_TEST_CONFIG' => array(
        'db_type'  => 'sqlsrv',
        'db_user'  => 'sa',
        'db_pwd'   => '1qaz@WSX',
        'db_host'  => '121.251.253.230',
        'db_port'  => '1433',
        'db_name'  => 'TMSSD_Test'
    ),
    'DB_SQLSERVER_TMSSD_WEIXIN_CONFIG' => array(
        'db_type'  => 'sqlsrv',
        'db_user'  => 'weixin',
        'db_pwd'   => 'weixinupol',
        'db_host'  => '121.251.253.214',
        'db_port'  => '1433',
        'db_name'  => 'TMSSD'
    ),
    'DB_SQLSERVER_TMSSD_CONFIG' => array(
        'db_type'  => 'sqlsrv',
        'db_user'  => 'sa',
        'db_pwd'   => '1qaz!@#$',
        'db_host'  => '121.251.253.214',
        'db_port'  => '1433',
        'db_name'  => 'TMSSD'
    ),
);
