<?php
namespace Home\Controller;
use Think\Controller;
use Com\Wxpay\lib\WxPayConfig;
use Com\Wxpay\example\JsApiPay;

// +----------------------------------------------------------------------
// |===== Pyboon =====
// +----------------------------------------------------------------------
// | Copyright (c) 2016 Pyboon All rights reserved.
// +----------------------------------------------------------------------
// | Author: Baboon <Pyboon@foxmail.com>
// +----------------------------------------------------------------------
// | Date: 2016年2月18日 下午4:22:11
// +----------------------------------------------------------------------
class CheckInController extends Controller{
    
    
    public function generateCode(){
    
        if(IS_POST){
            $return_data = array();
            $UserName = I("UserName");
            $pwd = I("pwd");
            $model = D("FMSCommon");
            $model->setTableName("gUser");
            $teacher = $model->where(array("UserName"=>$UserName))->find();
            if($teacher){
                /* if($class["ClassTeacherName"]==$ClassTeacherName){
                    $model->setTableName("gUser");
                    $teacher = $model->where(array("Id"=>$class["ClassTeacherID"]))->find();
                    if($teacher["Password"] == strtoupper(md5($pwd))){
                        $data = 'http://'.$_SERVER['HTTP_HOST']."/sdcenWX/index.php/Home/CheckIn/info/ClassID/".$ClassID;
                        qrcode($data, $ClassID.".png");
                        $return_data["status"] = 200;
                        $return_data["url"] = "/sdcenWX/Public/Img/".$ClassID.".png";
                        $return_data["message"] = "success";
                    }
                    else {
                        $return_data["status"] = 302;
                        $return_data["message"] = "班主任密码不正确";
                    }
                    
                }
                else {
                    $return_data["status"] = 301;
                    $return_data["message"] = "班主任信息不正确";
                } */
                
                if($teacher["Password"]==strtoupper(md5($pwd))){
                    $model->setTableName("FBase_ClassInfo");
                    $class = $model->where(array("ClassTeacherID"=>$teacher["Id"]))->select();
                    if(count($class)>1){
                        foreach ($class as $key => $value) {
                            $ClassID = $value["Id"];
                            $data = 'http://'.$_SERVER['HTTP_HOST']."/sdcenWX/index.php/Home/CheckIn/info/ClassID/".$ClassID;
                            qrcode($data, $ClassID.".png");
                            $class[$key]['url'] = "/sdcenWX/Public/Img/".$ClassID.".png";
                        }
                        $return_data["status"] = 201;
                        $return_data["message"] = "多个班级";
                        $return_data["classes"] = $class;
                    }
                    else {
                        $ClassID = $class["Id"];
                        $data = 'http://'.$_SERVER['HTTP_HOST']."/sdcenWX/index.php/Home/CheckIn/info/ClassID/".$ClassID;
                        qrcode($data, $ClassID.".png");
                        $return_data["status"] = 200;
                        $return_data["url"] = "/sdcenWX/Public/Img/".$ClassID.".png";
                        $return_data["message"] = "success";
                    }
                }
                else {
                    $return_data["status"] = 302;
                    $return_data["message"] = "班主任密码不正确".md5($pwd);
                }
                
            }
            else {
                $return_data["status"] = 300;
                $return_data["message"] = "班主任信息不存在";
            }
            $this->ajaxReturn($return_data);
            
        }
        else {
            $model = D("FMSCommon");
            $model->setTableName("FBase_ClassInfo");
            $class = $model->where("IsReport=1")->select();
            $this->assign("class",$class);
            $this->display();
        }
    
    }
    public function index(){//报到输入cardNum界面
     //接收班级Id，查询显示班级信息
     //header("Content-type: text/html; charset=utf-8");
     $ClassID = I("ClassID");//"a6c1941d-87bd-4e94-b532-6556b9c986cd";
     $model = D("FMSCommon");
     $model->setTableName("FBase_ClassInfo");
     $class = $model->where(array("Id"=>$ClassID))->find();
     
     $openid = "453ajgi3q58tjdfe";
     $this->assign("openid",$openid);
     $this->assign("ClassID",$ClassID);
     $this->assign("class",$class);
     $this->display();
    } 
    
    
    public function info(){
        
        $openid = $this->getOpenid();
        $model = D("FMSCommon");
        $model->setTableName("FBase_StuInfo");
        $data["WeiXin"] = $openid;
        $res = $model->where($data)->find();
        $ClassID = I("ClassID");
        if($res){
            $this->assign("openid",$openid);
            
            $model->setTableName("View_Base_StuInfo");
            $stu = $model->where(array("WeiXin"=>$openid,"ClassID"=>$ClassID))->find();
            if($stu){
                //该班存在该学生
                $this->assign("stu",$stu);
            }
            else {
                //绑定过的用户，但是本班级没有该学员
            }
        }
        else {
            //$url = $_SERVER['HTTP_HOST']."/sdcenWX/index.php/Home/CheckIn/info?classid=".$ClassID;
            //session("classid",$ClassID);
            $this->redirect('/Home/CheckIn/bind', array('classid' => $ClassID));
            //$this->redirect($url);
        }
        $this->display();
    }
    
    
    public function checkin(){
        $openid = I("openid");
        $ClassID = I("ClassID");
        $StuID = I("StuID");
        $model = D("FMSCommon");
        $return_data = array();
        $model->setTableName("FBase_ClassStuInfo");
        $data["IsReport"] = 1;
        $data["ReportDate"] = date("Y-m-d H:i:s");
        $stu = $model->where(array("ClassID"=>$ClassID,"StuID"=>$StuID))->save($data);
      /*   print_r($ClassID);
        print_r($model->getLastSql()); */
        if($stu){
            
            $return_data["status"] = 200;
            $return_data["url"] = U("Home/CheckIn/success");
            $return_data["message"] = "success";
            $this->ajaxReturn($return_data);
        }
        else {
            $return_data["status"] = 300;
            $return_data["message"] = "fail";
            $this->ajaxReturn($return_data);
        }
    }
    
    public function success(){
        $this->display();
    }
    /**
    * @description:
    * @params:报到统计
    * @return:
    * @author: baboon
    **/
    public function statistic(){
        if(IS_POST){
           $return_data = array();
            $UserName = I("UserName");
            $pwd = I("pwd");
            $model = D("FMSCommon");
            $model->setTableName("gUser");
            $teacher = $model->where(array("UserName"=>$UserName))->find();
            if($teacher){
                if($teacher["Password"] == strtoupper(md5($pwd))){
                    $model->setTableName("FBase_ClassInfo");
                    $class = $model->where(array("ClassTeacherID"=>$teacher["Id"],"IsReport"=>1))->select();
                    if(count($class)>1){
                        foreach ($class as $key => $value) {
                            $class[$key]["url"] = U("statisticpage",array("ClassID"=>$value["Id"]));
                        }
                        $return_data["status"] = 201;
                        $return_data["classes"] = $class;
                    }
                    else {
                        $return_data["status"] = 200;
                        $return_data["url"] = U("statisticpage",array("ClassID"=>$class["Id"]));
                    }
                    
                }
                else {
                    $return_data["status"] = 302;
                    $return_data["message"] = "账号密码不正确";
                }
            }
            else {
                $return_data["status"] = 301;
                $return_data["message"] = "教师信息不存在";
            }
            
            $this->ajaxReturn($return_data);
        }
        else {
            /* $model = D("FMSCommon");
            $model->setTableName("FBase_ClassInfo");
            $class = $model->where("IsReport=1")->select();
            $this->assign("class",$class); */
            $this->display("statisticLogin");
        }
        
    }
    
    public function statisticpage(){
        $ClassID = I("ClassID");
        if(!empty($ClassID)){
            $model = D("FMSCommon");
            $model->setTableName("View_Base_StuInfo");
            $stu = $model->where(array("ClassID"=>$ClassID))->order("IsReport desc")->select();
            $this->assign("sum",count($stu));
            $sum = $model->where(array("ClassID"=>$ClassID))->field(array("IsReport", "count(IsReport)"=>"sum"))->group('IsReport')->select();
            if($sum[0]["IsReport"]==0){
                $noReport = $sum[0]["sum"];
                $this->assign("noReport",$noReport);
                $this->assign("hasReport",count($stu)-$noReport);
            }
            else if($sum[0]["IsReport"]==1){
                $hasReport = $sum[0]["sum"];
                $this->assign("hasReport",$hasReport);
                $this->assign("noReport",count($stu)-$noReport);
            }
            $this->assign("stus",$stu);
        }
        else {
            $this->assign("err",2);//无此班级
        }
        
        $this->display("statistic");
    }
    public function bind(){
        if(IS_POST){
            $return_data = array();
            $CardNum = I("CardNum");
            $openid = I("openid");
            /* $classID = session("classid");
            $this->assign("classid",$classID); */
            if(!empty($CardNum) && !empty($openid)){
                $model = D("FMSCommon");
                $model->setTableName("FBase_StuInfo");
                $data["WeiXin"] = $openid;
                $stu = $model->where(array("CardNum"=>$CardNum))->save($data);
                if($stu){
                
                    $return_data["status"] = 200;
                    $return_data["message"] = "success";
                    $classID = I("classid");
                    if(!empty($classID)){
                        $url = $_SERVER['HTTP_HOST']."/sdcenWX/index.php/Home/CheckIn/info/ClassID/".$classID;
                        $return_data["url"] = $url;
                    }
                    else {
                        $return_data["url"] = U("Home/CheckIn/bind");
                    }
                }
                else {
                    $return_data["status"] = 301;
                    $return_data["message"] = "fail";
                }
            }
            else {
                $return_data["status"]= 300;
                $return_data["message"] = "No openid or No cardNum";
            }
             $this->ajaxReturn($return_data);   
            
        }
        else {
            $openid = $this->getOpenid();
            $model = D("FMSCommon");
            $model->setTableName("View_Base_StuInfo");
            $stu = $model->where(array("weixin"=>$openid))->select();
            if($stu){
                $this->assign("isBind",true);
                $this->assign("user",$stu[0]);
                $this->assign("proj",$stu);
                $this->assign("openid",$openid);
            }
            else {
                $this->assign("isBind",false);
                $this->assign("openid",$openid);
            }
            $this->display();
        }
        
        
    }
    
    
    public function unbind(){
        $openid = I("openid");
        $return_data = array();
        if(!empty($openid)){
            $model = D("FMSCommon");
            $model->setTableName("FBase_StuInfo");
            $user = $model->where(array("WeiXin"=>$openid))->find();
            if($user){
                $model->where(array("WeiXin"=>$openid))->setField("WeiXin",null);
                $return_data["status"] = 200;
                $return_data["message"] = "解绑成功";
                $return_data["url"] = U("Home/CheckIn/bind");
            }
            else {
                $return_data["status"] = 301;
                $return_data["message"] = "无user";
            }
    
        }
        else {
            $return_data["status"] = 300;
            $return_data["message"] = "无openid";
        }
        $this->ajaxReturn($return_data);
    }
    
    public function modify(){
        $CardNum = I("CardNum");
        $post_data = I("post.");
        $return_data = array();
        unset($post_data["CardNum"]);
        $model = D("FMSCommon");
        $model->setTableName("FBase_StuInfo");
        if(!empty($CardNum)){
            $res = $model->where(array("CardNum"=>$CardNum))->save($post_data);
            if($res){
                $return_data["status"] = 200;
                $return_data["message"] = "修改成功"; 
            }
            else {
                $return_data["status"] = 300;
                $return_data["message"] = "修改失败";
            }
        }
        else{
            $return_data["status"] = 301;
            $return_data["message"] = "CardNum为空";
        }
        $this->ajaxReturn($return_data);
    }
    
    protected function getOpenid(){
        if(!empty($_SESSION["openid"]))
        {
            return $_SESSION["openid"];
        }
        else {
            $options = array(
                'appid'=>C("Non_Academic_WXAPPID"),
                'secret'=>C("Non_Academic_WXAPPSECRET"),
            );
            $wxpaycfg = new WxPayConfig($options);
            $tools = new JsApiPay($wxpaycfg);
            $openid = $tools->GetOpenid();
            session("openid",$openid);
            return $openid;
        }
        
    }
    
    public function onlineWork(){
        $openid = $this->getOpenid();
        $this->assign("openid",$openid);
        if($openid){
            /* $user = D("User")->getbyOpenid($openid);
            if($user){
                $model = D("FMSCommon");
                $model->setTableName("gUser");
                $gUser = $model->where(array("UserName"=>$user["UserName"],"Password"=>md5($user["Password"])))->find();
                if($gUser){
                    $base = "http://121.251.253.235:81/mobile/mobileLogin/wxlogin?";
                    $url = $base."username=".$user["UserName"]."&pwd=".$user["Password"];
                    //http://121.251.253.235:81/mobile/mobileLogin/wxlogin?username=201601201601180413&pwd=180413
                    Header("Location:".$url);
                }
                else {  ///已经绑定但是用户已经修改过密码
                    $this->assign("stu",$user);
                    $this->display();
                }
            }
            else {
                $this->display();
            } */
            $model = D("FMSCommon");
            $model->setTableName("FBase_StuInfo");
            $data["WeiXin"] = $openid;
            $res = $model->where($data)->find();
            if($res){/////账户已经绑定
                $usename = $res["CardNum"];
                $pwd = substr($usename,-6);///默认身份证后六位为初始密码
                ////查找gUser表判断密码是否正确，
                $model->setTableName("gUser");
                $user = $model->where(array("UserName"=>$usename,"Password"=>md5($pwd)))->find();
                if($user){
                    $base = "http://121.251.253.235:81/mobile/mobileLogin/wxlogin?";
                    $url = $base."username=".$usename."&pwd=".$pwd;
                    //http://121.251.253.235:81/mobile/mobileLogin/wxlogin?username=201601201601180413&pwd=180413
                    Header("Location:".$url);
                }
                else {///默认初始密码已经修改
                    $user = D("User")->getbyOpenid($openid);
                    $this->assign("stu",$res);
                    $this->display();
                }
                
            }
            else {
                $this->redirect("CheckIn/bind");
            }
            
        }
        
    }
    /**
    * @description:截取图片函数
    * @params:
    * @return:
    * @author: baboon
    **/
    public function img(){
        
        $this->display();
    }
    
    
   
}