<?php
namespace Home\Controller;
use Think\Controller;
use Com\Wxpay\lib\WxPayConfig;
use Com\Wxpay\example\JsApiPay;

// +----------------------------------------------------------------------
// |===== Pyboon =====
// +----------------------------------------------------------------------
// | Copyright (c) 2016 Pyboon All rights reserved.
// +----------------------------------------------------------------------
// | Author: Baboon <Pyboon@foxmail.com>
// +----------------------------------------------------------------------
// | Date: 2016年3月12日 下午4:22:11
// +----------------------------------------------------------------------
class NoticeController extends Controller{
    
    
    public function index(){
        
    } 
    
    /**
    * @description:成绩查询
    * @params:
    * @return:
    * @author: baboon
    **/
    public function score($openid){
        
        /* $return = array();
        $options = array(
            'appid'=>C("WXAPPID"),
            'secret'=>C("WXAPPSECRET"),
        );
        $wxpaycfg = new WxPayConfig($options);
        $tools = new JsApiPay($wxpaycfg); */
        //$openid = $tools->GetOpenid();
        $return = array();
        $return[0]["Title"] = "您的成绩：";
        $myusermodel = D("User");
        $myUser = $myusermodel->where(array("openid"=>$openid))->find();
        if($myUser){
            $scoremodel = D("TMSSDCommon");
            $scoremodel->setTableName("View_StuArchive_StuScoreInfo");
            $score = $scoremodel->where(array("StuNo"=>$myUser["username"]))->select();
            foreach ($score as $key => $value) {
                if($key<6){
                    $return[1]["Title"] .= $value["CourseName"].":".$value["GeneralScore"]."分\n";
                   // $return[1]["Description"] = $value["GeneralScore"];
                }
                else {
                    
                }
                
            }
            if(!empty($score)) {
                return $return;
            }
            else {
                return "暂无成绩";
            }
        }
        else {
            //先绑定账户
            return sprintf('请先<a href="%s">绑定账户</a>',"http://".$_SERVER['HTTP_HOST'].U("Home/Base/bindUser"));
        }
    }
    
    /**
    * @description:考场安排查询
    * @params:
    * @return:
    * @author: baboon
    **/
    public function examArrange($openid){
        header("Content-type: text/html; charset=utf-8");
        $return = array();
        $return[0]["Title"] = "您的考场安排：";
        /* $options = array(
            'appid'=>C("WXAPPID"),
            'secret'=>C("WXAPPSECRET"),
        );
        $wxpaycfg = new WxPayConfig($options);
        $tools = new JsApiPay($wxpaycfg);
        $openid = $tools->GetOpenid();*/
        $myusermodel = D("User");
        $myUser = $myusermodel->where(array("openid"=>$openid))->find(); 
        /* $stuid = I("stuid","16316104020");
        $myUser["username"] = $stuid; */
        if($myUser){
            $examsmodel = D("TMSSDCommon");
            $examsmodel->setTableName("View_Exam_RoomCourseStuInfo");
            $exams = $examsmodel->where(array("StuNo"=>$myUser["username"]))->select();
            foreach ($exams as $key => $value) {
                if($key<6){
                    $return[$key+1]["Title"] = $value["CourseName"].$value["Title"];
                    $return[$key+1]["Description"] = $value["Place"].$value["Name"]."\n".$value["BeginTime"]."-".$value["EndTime"];
                    $return[$key+1]["PicUrl"] = "";
                    $return[$key+1]["Url"] = "";
                }
                else {
                    
                }
                
            }
            if(!empty($exams)) {
                 //  dump($return);
                   return $return;
               }
               else {
                   return "暂无考试";
               }
        }
        else {
            //先绑定账户
            return sprintf('请先<a href="%s">绑定账户</a>',"http://".$_SERVER['HTTP_HOST'].U("Home/Base/bindUser"));
        }
    }

    /**
    * @description:学员查看通知
    * @params:
    * @return:
    * @author: baboon
    **/
    public function queryNotice($openid){
        header("Content-type: text/html; charset=utf-8");
        $notice = array();
        $notice[0]["Title"] = "您的通知：";
        /* $options = array(
            'appid'=>C("WXAPPID"),
            'secret'=>C("WXAPPSECRET"),
        );
        $wxpaycfg = new WxPayConfig($options);
        $tools = new JsApiPay($wxpaycfg);
        $openid = $tools->GetOpenid();*/
        $myusermodel = D("User");
        $myUser = $myusermodel->where(array("openid"=>$openid))->find(); 
        //$myUser["stuid"] = '9b76e0af-a47b-404e-a84e-b12f3390bd07';
        if($myUser){
            $noticemodel = D("TMSSDCommon");
            $noticemodel->setTableName("Common_NoticeUserInfo");
            $noticeuser = $noticemodel->where(array("OwnerID"=>$myUser["stuid"]))->select();
            foreach ($noticeuser as $key => $value) {
                if($key<6){
                    $noticemodel->setTableName("Common_NoticeInfo");
                    $content = $noticemodel->where(array("Id"=>$value["NoticeID"]))->find();
                    $notice[$key+1]["Title"] = $content["Title"].$content["Content"];
                    $notice[$key+1]["Description"] = $content["Content"];
                }
                else {
                    
                }
                
                
            }
           if(!empty($noticeuser)) {
               return $notice;
           }
           else {
               return "暂无通知";
           }
           
        }
        else {
            //先绑定账户
            return sprintf('请先<a href="%s">绑定账户</a>',"http://".$_SERVER['HTTP_HOST'].U("Home/Base/bindUser"));
        }
    }
    
}