<?php
namespace Home\Controller;
use Think\Controller;
use Com\Wechat\TPWechat;
use Com\Wechat\Wechat;
// +----------------------------------------------------------------------
// |===== Pyboon =====
// +----------------------------------------------------------------------
// | Copyright (c) 2016 Pyboon All rights reserved.
// +----------------------------------------------------------------------
// | Author: Baboon <Pyboon@foxmail.com>
// +----------------------------------------------------------------------
// | Date: 2016年1月18日 下午1:55:41
// +----------------------------------------------------------------------
class WeixinController extends Controller{
    
    var $appid;
    var $appsecret;
    
    /** 
    * @describation 初始化
    * @param unknowtype
    * @return return_type
    * @author baboon
    * @date 2016年1月18日下午2:15:05
    */
    function _initialize(){
    
        $this->appid = C('Academic_WXAPPID');
        $this->appsecret = C('Academic_WXAPPSECRET');
    
        $this->options = array(
            'token'=>C("Academic_TOKEN"), //填写你设定的key
            'encodingaeskey'=>C("Academic_EncodingAESKey"), //填写加密用的EncodingAESKey
            'appid'=>$this->appid, //填写高级调用功能的app id
            'appsecret'=>$this->appsecret, //填写高级调用功能的密钥
        );
    }
    
    /** 
    * @describation 微信通信入口
    * @param unknowtype
    * @return return_type
    * @author baboon
    * @date 2016年1月18日下午2:16:30
    */
    public function index() {
         
        $weObj = new TPWechat($this->options);// 初始化TPWechat类
        $weObj->valid();//
        $rev = $weObj->getRev();
        $openID = $rev->getRevFrom();
        $type = $rev->getRevType();
        switch($type) {
            case Wechat::MSGTYPE_TEXT:
                $msg = $rev->getRevContent();
                switch ($msg) {
                    case "报名":
                        $return_msg = array(
                        "0"=>array(
                        'Title'=>'在线报名',
                        'Description'=>'微信在线报名',
                        'PicUrl'=>'http://www.sdcen.cn/themes/default/images/zxbm.png',
                        'Url'=>'http://weixin.yzhihui.me/sdcenWX/index.php/Home/Base/signUp'
                            ),
                            );
                        $weObj->news($return_msg)->reply();
                        break;
                    case "绑定":
                        $return_msg = array(
                        "0"=>array(
                            'Title'=>'绑定账户',
                            'Description'=>'微信公众号账户绑定',
                            'PicUrl'=>'http://huozhan-uploads.stor.sinaapp.com/account.jpg',
                            'Url'=>'http://weixin.yzhihui.me/sdcenWX/index.php/Home/Base/bindUser'
                            ),
                            );
                        $weObj->news($return_msg)->reply();
                        break;
                    case "作业":
                        $return_msg = array(
                        "0"=>array(
                            'Title'=>'在线学习',
                            'Description'=>'微信在线作业',
                            'PicUrl'=>'http://huozhan-uploads.stor.sinaapp.com/zuoye.jpg',
                            'Url'=>'http://weixin.yzhihui.me/sdcenWX/index.php/Home/Base/onlineWork'
                            ),
                            );
                        $weObj->news($return_msg)->reply();
                        break;
                    case "报到":
                        $return_msg = array(
                        "0"=>array(
                            'Title'=>'现场报到',
                            'Description'=>'微信现场报到',
                            'PicUrl'=>'http://huozhan-uploads.stor.sinaapp.com/zuoye.jpg',
                            'Url'=>'http://weixin.yzhihui.me/sdcenWX/index.php/Home/CheckIn/generateCode'
                            ),
                            );
                        $weObj->news($return_msg)->reply();
                        break;
                        
                    
                    case "统计":
                        $return_msg = array(
                        "0"=>array(
                        'Title'=>'现场报到统计入口',
                        'Description'=>'微信现场报到统计',
                        'PicUrl'=>'http://huozhan-uploads.stor.sinaapp.com/zuoye.jpg',
                        'Url'=>'http://weixin.yzhihui.me/sdcenWX/index.php/Home/CheckIn/statistic'
                            ),
                            );
                        $weObj->news($return_msg)->reply();
                        break;
                    case "成绩":
                        $score = A("Notice")->score($openID);
                        if(is_array($score)){
                            $weObj->news($score)->reply();
                        }
                        else {
                            $weObj->text($score)->reply();
                        }
                        
                        break;
                    case "考场安排":
                        $exam = A("Notice")->examArrange($openID);
                        if(is_array($exam)){
                            $weObj->news($exam)->reply();
                        }
                        else {
                            $weObj->text($exam)->reply();
                        }
                        
                        break;
                    case "通知":
                        $notice = A("Notice")->queryNotice($openID);
                        if(is_array($notice)){
                            $weObj->news($notice)->reply();
                        }
                        else {
                            $weObj->text($notice)->reply();
                        }
                        
                        break;
                    case "menu":
                        $newmenu = $this->newmenu();
                         $result = $weObj->createMenu($newmenu);
                         if($result){
                             $weObj->text("创建菜单成功")->reply();
                         }
                         else {
                             $weObj->text("创建菜单失败")->reply();
                         }
                        
                        break;
                    default:
                        $weObj->text("欢迎您关注微信公众号！")->reply();
                        break;
                        
                    }
                exit;
                break;
            case Wechat::MSGTYPE_EVENT://事件推送
                $event = $rev->getRevEvent();
                switch($event['event']){
                     
                    case Wechat::EVENT_LOCATION:
                        $location = $rev->getRevEventGeo();
                        //S($openID,$location);
                       // $weObj->text("纬度：".$location['x']."\n经度：".$location['y'])->reply();
                        exit;
                        break;
                    case Wechat::EVENT_MENU_CLICK:
                        switch ($event["key"]) {
                            
                            case "myNotice":
                                $notice = A("Notice")->queryNotice($openID);
                                if(is_array($notice)){
                                    $weObj->news($notice)->reply();
                                }
                                else {
                                    $weObj->text($notice)->reply();
                                }
                                exit;
                                break;
                                
                            case "examArrange":
                                $exam = A("Notice")->examArrange($openID);
                                if(is_array($exam)){
                                    $weObj->news($exam)->reply();
                                }
                                else {
                                    $weObj->text($exam)->reply();
                                }
                                exit;
                                break;
                                
                            case "score":
                                $score = A("Notice")->score($openID);
                                if(is_array($score)){
                                    $weObj->news($score)->reply();
                                }
                                else {
                                    $weObj->text($score)->reply();
                                }
                                exit;
                                break;
                                
                            case "arranges"://行程安排
                                $model = D("FMSCommon");
                                $model->setTableName("FBase_StuInfo");
                                $data["WeiXin"] = $openID;
                                $res = $model->where($data)->find();
                                if($res){
                                    $return_msg = array(
                                        "0"=>array(
                                            'Title'=>'学员行程安排',
                                            'Description'=>'学员行程安排',
                                            'PicUrl'=>'http://sushan123-uploads.stor.sinaapp.com/arrange.jpg',
                                            'Url'=>'http://mp.weixin.qq.com/s?__biz=MzAwNDM3ODk5MQ==&mid=402172937&idx=1&sn=7f5a0fe63c2f33ef2208f8b56f3d0d6a#rd'
                                        ),
                                    );
                                    $weObj->news($return_msg)->reply();
                                }
                                
                                else {
                                    $str = sprintf('请先<a href="%s">绑定账户</a>',"http://".$_SERVER['HTTP_HOST'].U("Home/CheckIn/bind"));
                                    $weObj->text($str)->reply();
                                }
                                exit;
                                break;
                                
                        }
                        exit;
                        break;
                        
              
                }
    
                break;
            case Wechat::MSGTYPE_IMAGE:
    
                break;
            default:
                $weObj->text("help info")->reply();
        }
         
    
         
    }
    
    public function newmenu(){
        $newmenu =  array (
            'button' => array (
                0 => array (
                    'type'=>'view',
                    'name'=>'在线报名',
                    'url'=>"http://weixin.yzhihui.me/sdcenWX/index.php/Home/Base/signUp",
                ),
                1 => array (
                    'type'=>'view',
                    'name'=>'在线学习',
                    'url'=>"http://weixin.yzhihui.me/sdcenWX/index.php/Home/Base/onlineWork",
                ),
                2 => array (
                    'name' => '个人中心',
                    'sub_button' => array (
                        0 => array (
                            'type'=>'view',
                            'name'=>'绑定账户',
                            'url'=>"http://weixin.yzhihui.me/sdcenWX/index.php/Home/Base/bindUser",
                        ),
                        1 => array (
                            'type'=>'click',
                            'name'=>'我的通知',
                            'key'=>"myNotice",
                        ),
                        2 => array (
                            'type'=>'click',
                            'name'=>'考场安排',
                            'key'=>"examArrange",
                        ),
                        3 => array (
                            'type'=>'click',
                            'name'=>'成绩查询',
                            'key'=>"score",
                        ),
                    ),
                )
            ),
        );
        
        return $newmenu;
    }
    
    
}