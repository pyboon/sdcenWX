<?php
// +----------------------------------------------------------------------
// |===== Pyboon =====
// +----------------------------------------------------------------------
// | Copyright (c) 2016 Pyboon All rights reserved.
// +----------------------------------------------------------------------
// | Author: Baboon <Pyboon@foxmail.com>
// +----------------------------------------------------------------------
// | Date: 2016年1月20日 下午5:08:17
// +----------------------------------------------------------------------

namespace Home\Model;
use Think\Model;

/**
 * 
 */

class BaseSubInfoModel extends Model{

    protected $trueTableName = 'Base_SubInfo';
    protected $connection = 'DB_SQLSERVER_TMSSD_TEST_CONFIG';

}