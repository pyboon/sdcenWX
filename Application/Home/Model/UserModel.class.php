<?php
// +----------------------------------------------------------------------
// |===== Pyboon =====
// +----------------------------------------------------------------------
// | Copyright (c) 2016 Pyboon All rights reserved.
// +----------------------------------------------------------------------
// | Author: Baboon <Pyboon@foxmail.com>
// +----------------------------------------------------------------------
// | Date: 2016年1月20日 下午5:08:17
// +----------------------------------------------------------------------

namespace Home\Model;
use Think\Model;

/**
 * 
 */

class UserModel extends Model{
    
    protected $_auto = array(
        array('create_time', 'time', self::MODEL_INSERT, 'function'),
        array('status', 1, self::MODEL_INSERT)
    );

}