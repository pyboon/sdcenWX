<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
        <title>绑定账号</title>
        <link rel="stylesheet" href="/sdcenWX/Public/Home/css/weui.css"/>
        
        <style>
		.page, body {
		    background-color: #FBF9FE;
		}
		.weui_label {
		    display: block;
		    width: 4.7em;
		}
        </style>
    </head>
    <body>
       
        <div class="page">
        <?php if($isBind == true): ?><div class="hd">
                <h4 class="page_title" style="text-center">您已经绑定以下账号：</h4>
            </div><?php endif; ?>
            <div class="bd">
            <?php if($isBind == true): ?><div class="weui_cells_title">账号信息</div>
		        <div class="weui_cells">
		            
		            <div class="weui_cell">
		                <div class="weui_cell_bd weui_cell_primary">
		                    <p>账号：</p>
		                </div>
		                <div class="weui_cell_ft"><?php echo ($user["StuNo"]); ?></div>
		            </div>
		            <div class="weui_cell">
		                <div class="weui_cell_bd weui_cell_primary">
		                    <p>姓名：</p>
		                </div>
		                <div class="weui_cell_ft"><?php echo ($user["Name"]); ?></div>
		            </div>
		            <div class="weui_cell">
		                <div class="weui_cell_bd weui_cell_primary">
		                    <p>学校：</p>
		                </div>
		                <div class="weui_cell_ft"><?php echo ($user["SchoolName"]); ?></div>
		            </div>
		            <div class="weui_cell">
		                <div class="weui_cell_bd weui_cell_primary">
		                    <p>站点：</p>
		                </div>
		                <div class="weui_cell_ft"><?php echo ($user["SubName"]); ?></div>
		            </div>
		            <div class="weui_cell">
		                <div class="weui_cell_bd weui_cell_primary">
		                    <p>专业：</p>
		                </div>
		                <div class="weui_cell_ft"><?php echo ($user["MajorName"]); ?></div>
		            </div>
		        </div>
		        <div class="weui_btn_area">
                    <button class="weui_btn weui_btn_warn" id="unbind">解绑账号</button>
            </div>
		        <?php else: ?>
				<form id="form">
				<input  type="hidden" name="openid" value="<?php echo ($openid); ?>"/>
                <div class="weui_cells_title">账号信息</div>
                <div class="weui_cells">
                <div class="weui_cell weui_cell_select weui_select_after">
                        <div class="weui_cell_hd"><label class="weui_label">学校:</label></div>
			                <div class="weui_cell_bd weui_cell_primary">
			                    <select class="weui_select" name="schoolCode">
			                     <option value="">请选择学校</option>
			                    <?php if(is_array($school)): $i = 0; $__LIST__ = $school;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["Code"]); ?>"><?php echo ($vo["Name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
			                    </select>
			                </div>
                 </div>
                 <div class="weui_cell">
                     <div class="weui_cell_hd"><label class="weui_label">账号:</label></div>
                     <div class="weui_cell_bd weui_cell_primary">
                         <input class="weui_input" type="text" name="username" placeholder="请输入账号"/>
                     </div>
                 </div>
                 <div class="weui_cell">
                     <div class="weui_cell_hd"><label class="weui_label">密码:</label></div>
                     <div class="weui_cell_bd weui_cell_primary">
                         <input class="weui_input" type="password" name="pwd" placeholder="请输入密码"/>
                     </div>
                 </div>
                  
                </div>
        		<div class="weui_cells weui_cells_checkbox">
		            <label class="weui_cell weui_check_label" for="allow">
		                <div class="weui_cell_hd">
		                    <input type="checkbox" class="weui_check" name="allow" id="allow" checked="checked">
		                    <i class="weui_icon_checked"></i>
		                </div>
		                <div class="weui_cell_bd weui_cell_primary">
		                    <p>允许公众号绑定您的账号</p>
		                </div>
		            </label>
        		</div>
           </div>
			</form>
      		<div class="weui_btn_area">
                    <button class="weui_btn weui_btn_primary" id="submit">绑定账号</button>
            </div><?php endif; ?>  
            <div class="weui_toptips weui_warn" id="error" style="dispalay:none;"><span id="errMsg"></span></div>
			<!--BEGIN dialog1-->
		    <div class="weui_dialog_confirm" id="dialog" style="display: none;">
		        <div class="weui_mask"></div>
		        <div class="weui_dialog">
		            <div class="weui_dialog_hd"><strong class="weui_dialog_title">解绑确认</strong></div>
		            <div class="weui_dialog_bd">是否解除账号绑定？</div>
		            <div class="weui_dialog_ft">
		                <a href="javascript:;" class="weui_btn_dialog default" id="cancel">取消</a>
		                <a href="javascript:;" class="weui_btn_dialog primary" id="confirm">确定</a>
		            </div>
		        </div>
		    </div>
		    <!--END dialog1-->
     </div>
        
    <!-- <script src="http://cdn.bootcss.com/jquery/1.11.1/jquery.min.js"></script> -->
    <script type='text/javascript' src='//g.alicdn.com/sj/lib/zepto/zepto.min.js' charset='utf-8'></script>
    <script>
     $(function(){
    	 
		$("input[name=allow]").change(function(){
			if($(this).is(":checked")){
				$("#submit").css("background","#04BE02");
				$("#submit").removeAttr("disabled");//将按钮可用
			}
			else {
				$("#submit").css("background","gray");
				$("#submit").attr("disabled", true);
			}
    	 });
    	 
    	 $("#submit").click(function(){
			    if(checkParams()){
			    	bind();
			    	
			    }		 
    	 });
    	 $("#unbind").click(function(){
			   $("#dialog").show();  
 	 	});
    	 $("#cancel").click(function(){
      		
			   $("#dialog").hide();  
	 	});
    	 $("#confirm").click(function(){
    		 $.ajax({
       			type:"POST",
       			url:"/sdcenWX/index.php/Home/Base/unbind",
       			data:{openid:'<?php echo ($openid); ?>'},
       			dataType:"json",
       			success:function(res){
       				if(res.status==200){
       					window.location.href = res.url;
       				}
       				else {
       					alert(res.message);
       				}
       			},
       			error:function(){
       				alert("err");
       			}
       		});
			   
	 	});
    	 
    	 function bind(){
    		 $.ajax({
      			type:"POST",
      			url:"/sdcenWX/index.php/Home/Base/insertBind",
      			data:$("#form").serialize(),
      			dataType:"json",
      			success:function(res){
      				if(res.status==200){
      					window.location.href = res.url;
      				}
      				else {
      					alert(res.message);
      				}
      			},
      			error:function(){
      				alert("err");
      			}
      		});
    		 
    	 }
     });
       
       
       
       function checkParams(){
    	   if($("select[name=schoolCode]").val().length==0){
    		   errShow("请选择学校");
    		   return false;
    	   }
    	   if($("input[name=username]").val().length==0){
    		   errShow("请填写用户名");
    		   return false;
    	   }
    	   if($("input[name=pwd]").val().length==0){
    		   errShow("请填写密码");
    		   return false;
    	   }
    	   return true;
       }
       
       function errShow(msg){
    	   var elem = $("#error")
    	   $("#errMsg").text(msg);
    	   elem.show();
    	   setTimeout(function(){
    		   elem.hide();
    	   },2000);
    	   /* var height = elem.height();
   		   var startCss = {top:0 - height}
           var endCss   = {top:0}
   		   elem.css(startCss).show().animate(endCss, 300)
   		   setTimeout(function () {
   			elem.animate(startCss, 500, function() {
   	            elem.hide()
   	        })
                }, 2000); */
   		
       }
    </script>
    </body>
</html>