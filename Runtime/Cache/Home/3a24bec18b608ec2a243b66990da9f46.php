<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
        <title>信息确认</title>
        <link rel="stylesheet" href="/sdcenWX/Public/Home/css/weui.css"/>
        
        <style>
		.page, body {
		    background-color: #FBF9FE;
		}
		.weui_label {
		    display: block;
		    width: 4.7em;
		}
        </style>
    </head>
    <body>
       <?php if(empty($stu) != true): ?><div class="page">
        
            <div class="hd">
                <h4 class="page_title" style="text-align:center;margin:5px auto;">您的报到信息如下：</h4>
            </div>
         
            <div class="bd">
            
            	<div class="weui_cells_title">学员信息</div>
		        <div class="weui_cells">
		            
		            <div class="weui_cell">
		                <div class="weui_cell_bd weui_cell_primary">
		                    <p>证件号：</p>
		                </div>
		                <div class="weui_cell_ft"><?php echo ($stu["CardNum"]); ?></div>
		            </div>
		            <div class="weui_cell">
		                <div class="weui_cell_bd weui_cell_primary">
		                    <p>姓名：</p>
		                </div>
		                <div class="weui_cell_ft"><?php echo ($stu["Name"]); ?></div>
		            </div>
		            <div class="weui_cell">
		                <div class="weui_cell_bd weui_cell_primary">
		                    <p>电话：</p>
		                </div>
		                <div class="weui_cell_ft"><?php echo ($stu["Phone"]); ?></div>
		            </div>
		            <div class="weui_cell">
		                <div class="weui_cell_bd weui_cell_primary">
		                    <p>公司：</p>
		                </div>
		                <div class="weui_cell_ft"><?php echo ($stu["CompanyName"]); ?></div>
		            </div>
		        </div>
		        <div class="weui_cells_title">培训信息</div>
		        <div class="weui_cells">
		            
		            <div class="weui_cell">
		                <div class="weui_cell_bd weui_cell_primary">
		                    <p>学校：</p>
		                </div>
		                <div class="weui_cell_ft"><?php echo ($stu["SchoolName"]); ?></div>
		            </div>
		            <div class="weui_cell">
		                <div class="weui_cell_bd weui_cell_primary">
		                    <p>项目：</p>
		                </div>
		                <div class="weui_cell_ft"><?php echo ($stu["PlanName"]); ?></div>
		            </div>
		            <div class="weui_cell">
		                <div class="weui_cell_bd weui_cell_primary">
		                    <p>班级：</p>
		                </div>
		                <div class="weui_cell_ft"><?php echo ($stu["ClassName"]); ?></div>
		            </div>
		        </div>
		        <div class="weui_btn_area">
                    <button class="weui_btn weui_btn_warn" id="checkin">确认报到</button>
            </div>
		          
            <div class="weui_toptips weui_warn" id="error" style="dispalay:none;"><span id="errMsg"></span></div>
			<!--BEGIN dialog1-->
		    <div class="weui_dialog_confirm" id="dialog" style="display: none;">
		        <div class="weui_mask"></div>
		        <div class="weui_dialog">
		            <div class="weui_dialog_hd"><strong class="weui_dialog_title">报到信息确认</strong></div>
		            <div class="weui_dialog_bd">是否确认您的报到信息，确认报到？</div>
		            <div class="weui_dialog_ft">
		                <a href="javascript:;" class="weui_btn_dialog default" id="cancel">取消</a>
		                <a href="javascript:;" class="weui_btn_dialog primary" id="confirm">确认</a>
		            </div>
		        </div>
		    </div>
		    <!--END dialog1-->
      </div>
     </div><?php endif; ?> 
    <!-- <script src="http://cdn.bootcss.com/jquery/1.11.1/jquery.min.js"></script> -->
    <script type='text/javascript' src='//g.alicdn.com/sj/lib/zepto/zepto.min.js' charset='utf-8'></script>
    <script>
    <?php if(empty($stu) != true): ?>$(function(){
    	 
    	 $("#checkin").click(function(){
			   $("#dialog").show();  
 	 	});
    	 $("#cancel").click(function(){
      		
			   $("#dialog").hide();  
	 	});
    	 $("#confirm").click(function(){
    		 $.ajax({
       			type:"POST",
       			url:"/sdcenWX/index.php/Home/CheckIn/checkin",
       			data:{openid:'<?php echo ($openid); ?>',ClassID:"<?php echo ($stu["ClassID"]); ?>",StuID:"<?php echo ($stu["Id"]); ?>"},
       			dataType:"json",
       			success:function(res){
       				if(res.status==200){
       					window.location.href = res.url;
       				}
       				else {
       					alert(res.message);
       				}
       			},
       			error:function(){
       				alert("err");
       			}
       		});
			   
	 	});
    	 
     });
    <?php else: ?>
    
    alert("该班没有您的信息，请您核对您的身份信息");
    history.go(-1);<?php endif; ?>
       
    </script>
    </body>
</html>