<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
        <title>二维码生成</title>
        <link rel="stylesheet" href="/sdcenWX/Public/Home/css/weui.css"/>
       <!--  <link rel="stylesheet" href="/sdcenWX/Public/Home/css/choosen.css"/> -->
        
        <style>
		.page, body {
		    background-color: #FBF9FE;
		}
		.weui_label {
		    display: block;
		    width: 4.7em;
		}
        </style>
    </head>
    <body>
       
        <div class="page">
            <div class="bd">
				<form id="form">
				<input  type="hidden" name="openid" value="<?php echo ($openid); ?>"/>
                <div class="weui_cells_title">账号信息</div>
                <div class="weui_cells">
                <div class="weui_cell weui_cell_select weui_select_after">
                        <div class="weui_cell_hd"><label class="weui_label">班级:</label></div>
			                <div class="weui_cell_bd weui_cell_primary">
			                    <select class="weui_select chosen" name="ClassID">
			                    <?php if(is_array($class)): $i = 0; $__LIST__ = $class;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["Id"]); ?>"><?php echo ($vo["Name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
			                    </select>
			                </div>
                 </div>
                 <div class="weui_cell">
                     <div class="weui_cell_hd"><label class="weui_label">班主任:</label></div>
                     <div class="weui_cell_bd weui_cell_primary">
                         <input class="weui_input" type="text" name="ClassTeacherName" placeholder="请输入班主任姓名"/>
                     </div>
                 </div>
                 <div class="weui_cell">
                     <div class="weui_cell_hd"><label class="weui_label">密码:</label></div>
                     <div class="weui_cell_bd weui_cell_primary">
                         <input class="weui_input" type="password" name="pwd" placeholder="请输入班主任账户密码"/>
                     </div>
                 </div>
               
           </div>
			</form>
      		<div class="weui_btn_area">
                    <button class="weui_btn weui_btn_primary" id="submit">生成报到二维码</button>
            </div>
            <div class="weui_toptips weui_warn" id="error" style="dispalay:none;"><span id="errMsg"></span></div>
			<!--BEGIN dialog1-->
		    <div class="weui_dialog_confirm" id="dialog" style="display: none;">
		        <div class="weui_mask"></div>
		        <div class="weui_dialog">
		            <div class="weui_dialog_hd"><strong class="weui_dialog_title">报到二维码</strong></div>
		            <div class="weui_dialog_bd">
		            <p style="text-align:center;"><img src="" id="code" ></p>
		            <p style="text-align:center;">请在微信中扫码</p>
		            </div>
		            <div class="weui_dialog_ft">
		                <a href="javascript:;" class="weui_btn_dialog primary" id="confirm">确定</a>
		            </div>
		        </div>
		    </div>
		    <!--END dialog1-->
     </div>
        
    <script type='text/javascript' src='/sdcenWX/Public/Home/js/jquery-2.1.1.min.js' charset='utf-8'></script>
    <script type='text/javascript' src='/sdcenWX/Public/Home/js/chosen.jquery.js' charset='utf-8'></script>	
    <script>
     $(function(){
    	 
		
    	/*  $(".weui_select").chosen(); */
    	 $("#submit").click(function(){
			    if(checkParams()){
			    	generate();
			    }		 
    	 });
    	 $("#confirm").click(function(){
			   $("#dialog").hide();  
	 	});
    	 
    	 function generate(){
    		 $.ajax({
      			type:"POST",
      			url:"/sdcenWX/index.php/Home/CheckIn/generateCode",
      			data:$("#form").serialize(),
      			dataType:"json",
      			success:function(res){
      				if(res.status==200){
      					//alert(res.url);
      					$("#code").attr("src",res.url);
      					$("#dialog").show();  
      				}
      				else {
      					alert(res.message);
      				}
      			},
      			error:function(){
      				alert("err");
      			}
      		});
    		 
    	 }
     });
       
       function checkParams(){
    	   if($("select[name=ClassID]").val().length==0){
    		   errShow("请选择班级");
    		   return false;
    	   }
    	   if($("input[name=ClassTeacherName]").val().length==0){
    		   errShow("请填写班主任姓名");
    		   return false;
    	   }
    	   if($("input[name=pwd]").val().length==0){
    		   errShow("请填写账户密码");
    		   return false;
    	   }
    	   return true;
       }
       
       function errShow(msg){
    	   var elem = $("#error")
    	   $("#errMsg").text(msg);
    	   elem.show();
    	   setTimeout(function(){
    		   elem.hide();
    	   },2000);
       }
    </script>
    </body>
</html>