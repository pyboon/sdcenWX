<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
        <title>报到统计</title>
        <link rel="stylesheet" href="/sdcenWX/Public/Home/css/weui.css"/>
        
        <style>
		.page, body {
		    background-color: #FBF9FE;
		}
		.weui_label {
		    display: block;
		    width: 4.7em;
		}
        </style>
    </head>
    <body>
       
        <div class="page">
        <?php if($err == 1): ?><script>
	        alert("班主任信息不正确");
	        history.go(-1);
	        </script>
        <?php elseif($err == 2): ?>
	        <script>
	        alert("无此班级");
	        history.go(-1);
	        </script>
        <?php else: ?>
        	<div class="hd">
                <h4 class="page_title" style="text-align:center;margin:5px auto;"><?php echo ($stus[0]["ClassName"]); ?></h4>
            </div>
            <div class="bd">
            
            	<div class="weui_cells_title">班级总人数:<span><?php echo ($sum); ?></span>/已报到:<span><?php echo ($hasReport); ?></span>/未报到:<span><?php echo ($noReport); ?></span></div>
		        <div class="weui_cells">
		            <?php if(is_array($stus)): $i = 0; $__LIST__ = $stus;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><div class="weui_cell">
		            	<div class="weui_cell_hd weui_cell_primary">
		                    <p><?php echo ($vo["Name"]); ?></p>
		                </div>
		                <div class="weui_cell_bd weui_cell_primary">
		                    <p><?php echo ($vo["DateCreated"]); ?></p>
		                </div>
		                <div class="weui_cell_ft">
		                <?php if($vo["IsReport"] == 1): ?><i class="weui_icon_success_no_circle"></i>
		                <?php else: ?>
		                <i class="weui_icon_warn"></i><?php endif; ?>
		                </div>
		            </div><?php endforeach; endif; else: echo "" ;endif; ?>
		        </div>
		        <!-- <div class="weui_btn_area">[IsValid]
                    <button class="weui_btn weui_btn_warn" id="unbind">确认报到</button>
            	</div> -->
		          
            <div class="weui_toptips weui_warn" id="error" style="dispalay:none;"><span id="errMsg"></span></div>
     	</div>
     	
     	<script type='text/javascript' src='//g.alicdn.com/sj/lib/zepto/zepto.min.js' charset='utf-8'></script>
    <script>
     $(function(){
    	 
		$("input[name=allow]").change(function(){
			if($(this).is(":checked")){
				$("#submit").css("background","#04BE02");
				$("#submit").removeAttr("disabled");//将按钮可用
			}
			else {
				$("#submit").css("background","gray");
				$("#submit").attr("disabled", true);
			}
    	 });
    	 
    	 $("#submit").click(function(){
			    if(checkParams()){
			    	bind();
			    	
			    }		 
    	 });
    	 $("#unbind").click(function(){
			   $("#dialog").show();  
 	 	});
    	 $("#cancel").click(function(){
      		
			   $("#dialog").hide();  
	 	});
    	 $("#confirm").click(function(){
    		 $.ajax({
       			type:"POST",
       			url:"/sdcenWX/index.php/Home/CheckIn/unbind",
       			data:{openid:'<?php echo ($openid); ?>'},
       			dataType:"json",
       			success:function(res){
       				if(res.status==200){
       					window.location.href = res.url;
       				}
       				else {
       					alert(res.message);
       				}
       			},
       			error:function(){
       				alert("err");
       			}
       		});
			   
	 	});
    	 
    	 function bind(){
    		 $.ajax({
      			type:"POST",
      			url:"/sdcenWX/index.php/Home/CheckIn/insertBind",
      			data:$("#form").serialize(),
      			dataType:"json",
      			success:function(res){
      				if(res.status==200){
      					window.location.href = res.url;
      				}
      				else {
      					alert(res.message);
      				}
      			},
      			error:function(){
      				alert("err");
      			}
      		});
    		 
    	 }
     });
       
       
       
       function checkParams(){
    	   if($("select[name=schoolCode]").val().length==0){
    		   errShow("请选择学校");
    		   return false;
    	   }
    	   if($("input[name=username]").val().length==0){
    		   errShow("请填写用户名");
    		   return false;
    	   }
    	   if($("input[name=pwd]").val().length==0){
    		   errShow("请填写密码");
    		   return false;
    	   }
    	   return true;
       }
       
       function errShow(msg){
    	   var elem = $("#error")
    	   $("#errMsg").text(msg);
    	   elem.show();
    	   setTimeout(function(){
    		   elem.hide();
    	   },2000);
   		
       }
    </script><?php endif; ?>
            
         
        
    
    </body>
</html>