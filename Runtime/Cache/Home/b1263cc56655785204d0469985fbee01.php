<?php if (!defined('THINK_PATH')) exit();?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
        <title>在线报名</title>
        <link rel="stylesheet" href="/sdcenWX/Public/Home/css/weui.css"/>
        
        <style>
		.page, body {
		    background-color: #FBF9FE;
		}
		.weui_label {
		    display: block;
		    width: 4.7em;
		}
        </style>
         <!--[if lt IE 9]>
        <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <![endif]-->
    </head>
    <body>
       
        <div class="page">
          <!--   <div class="hd">
                <h1 class="page_title">Cell</h1>
            </div> -->
            <div class="bd">
				<form id="form">
				
				<input  type="hidden" name="SchoolName" value="中国石油大学"/>
                <div class="weui_cells_title">个人信息(必填)</div>
                <div class="weui_cells">
                    <div class="weui_cell">
                        <div class="weui_cell_hd"><label class="weui_label">姓名:</label></div>
                        <div class="weui_cell_bd weui_cell_primary">
                            <input class="weui_input" type="text" name="Name" placeholder="请输入姓名"/>
                        </div>
                    </div>
                    <div class="weui_cell">
                        <div class="weui_cell_hd"><label class="weui_label">手机号:</label></div>
                        <div class="weui_cell_bd weui_cell_primary">
                            <input class="weui_input" type="tel" name="Phone" placeholder="请输入手机号"/>
                        </div>
                    </div>
                    
                    <div class="weui_cell weui_cell_select weui_select_after">
                        <div class="weui_cell_hd"><label class="weui_label">证件类型:</label></div>
			                <div class="weui_cell_bd weui_cell_primary">
			                    <select class="weui_select" name="CardType" id="CardType">
			                        <option value="身份证">身份证</option>
			                        <option value="军人证件">军人证件</option>
			                        
			                    </select>
			                </div>
                    </div>
                    <div class="weui_cell">
                        <div class="weui_cell_hd "><label class="weui_label">证件号:</label></div>
                        <div class="weui_cell_bd weui_cell_primary">
                            <input class="weui_input" type="number" name="CardNum" id="CardNum" placeholder="请输入证件号"/>
                        </div>
                    </div>
                </div>
                
                <div class="weui_cells_title">报考信息(可选填)</div>
                <div class="weui_cells">
                    <div class="weui_cell weui_cell_select weui_select_after">
                        <div class="weui_cell_hd"><label class="weui_label">报考站点:</label></div>
			                <div class="weui_cell_bd weui_cell_primary">
			                    <select class="weui_select" name="SubID">
			                    <?php if($sublist != null): if(is_array($sublist)): $i = 0; $__LIST__ = $sublist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["Id"]); ?>"><?php echo ($vo["Name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; endif; ?>   
			                    </select>
			                </div>
                    </div>
                    <div class="weui_cell weui_cell_select weui_select_after">
                        <div class="weui_cell_hd"><label class="weui_label">报考层次:</label></div>
			                <div class="weui_cell_bd weui_cell_primary">
			                    <select class="weui_select" name="StuLevel">
			                      
			                    </select>
			                </div>
                    </div>
                    <div class="weui_cell weui_cell_select weui_select_after">
                        <div class="weui_cell_hd"><label class="weui_label">报考专业:</label></div>
			                <div class="weui_cell_bd weui_cell_primary">
			                    <select class="weui_select" name="MajorID_LevelID">
			                    
			                    </select>
			                </div>
                    </div>
                    
                </div>
				</form>
            </div>

                <div class="weui_cells_tips">此处为预报名，只填报基本信息，现场确认信息时完成其他的信息填写</div>
                <div class="weui_btn_area">
                    <a class="weui_btn weui_btn_primary" id="submit">立即报名</a>
                </div>
                
                <div class="weui_toptips weui_warn" id="error" style="dispalay:none;"><span id="errMsg">测试</span></div>               
                
            </div>
        
    <script src="http://cdn.bootcss.com/jquery/1.11.1/jquery.min.js"></script>
    <script>
     $(function(){
    	 
    	 function getLevel(){
    		 var subID = $("select[name=SubID] option:selected").val()
     		// alert(subID);
     		 $.ajax({
      			type:"POST",
      			url:"/sdcenWX/index.php/Home/Base/getLevel",
      			data:{subID:subID},
      			dataType:"json",
      			success:function(res){
      				var  level = $("select[name=StuLevel]");
  					level.empty();
      				if(res.status==200){
      					var levellist = res.levellist;
      					
      					$.each(levellist,function(n,value){
      						var option = "<option value=\""+value.StuLevel+"\">"+value.StuLevel+"</option>";
      						level.append(option);
      					});
      					getMajor();//level载入结束之后首先载入专业，防止只有一个层次，无法触发change事件
      				}
      				else {
      					alert(res.message);
      				}
      			},
      			error:function(){
      				alert("err");
      			}
      		});
    	 }
    	 
    	 function getMajor(){
    		 var subID = $("select[name=SubID] option:selected").val()
    		 var StuLevel = $("select[name=StuLevel] option:selected").val()
    		
    		 $.ajax({
     			type:"POST",
     			url:"/sdcenWX/index.php/Home/Base/getMajor",
     			data:{subID:subID, StuLevel:StuLevel},
     			dataType:"json",
     			success:function(res){
     				var  major = $("select[name=MajorID_LevelID]");
 					major.empty();
     				if(res.status==200){
     					var majorlist = res.majorlist;
     					
     					$.each(majorlist,function(n,value){
     						var option = "<option value=\""+value.MajorID+"#"+value.StuLevelID+"\">"+value.MajorName+"</option>";
     						major.append(option);
     					
     					});
     				}
     				else {
     					alert(res.message);
     				}
     			},
     			error:function(){
     				alert("err");
     			}
     		});
    		 
    	 }
    	 getLevel();//载入就取层次--因为selelct只有一个option的时候无法触发change事件
    	 $("select[name=SubID]").change(function(){
    		 //选择站点改变时触发，调用getLevel();
    		 getLevel();
    	 });
		$("select[name=StuLevel]").click(function(){
			getMajor();
    	 });
    	 
    	 $("#submit").click(function(){
    		
			    if(checkParams()){
			    	//alert($("#form").serialize())
			    	signUp();
			    	
			    }		 
    	 });
    	 
    	 function signUp(){
    		
    		 $.ajax({
      			type:"POST",
      			url:"/sdcenWX/index.php/Home/Base/insertStuInfo",
      			data:$("#form").serialize(),
      			dataType:"json",
      			success:function(res){
      				if(res.status==200){
      					window.location.href = res.url;
      				}
      				else {
      					alert(res.message);
      				}
      			},
      			error:function(){
      				alert("err");
      			}
      		});
    		 
    	 }
     });
       
       
       
       function checkParams(){
    	   if($("input[name=Name]").val().length==0){
    		   errShow("请填写姓名");
    		   return false;
    	   }
    	   var phoneReg = /^1[3|4|5|7|8][0-9]\d{8}$/
    	   if(!phoneReg.test($("input[name=Phone]").val())){
    		   errShow("请填写正确的手机号");
    		   return false;
    	   }
    	   var idReg = /(^\d{15}$)|(^\d{17}([0-9]|X)$)/
    	   if($("#CardType option:selected").val()=="身份证"){
    		   if(!idReg.test($("#CardNum").val())){
    			   errShow("请填写正确的身份证号");
        		   return false;
    		   }
    	   }
    	   return true;
       }
       
       function errShow(msg){
    	   var elem = $("#error");
    	   $("#errMsg").text(msg);
    	   elem.show();
    	   setTimeout(function(){
    		   elem.hide();
    	   },2000);
    	  /*  var elem = $("#error")
    	   $("#errMsg").text(msg);
    	   var height = elem.height();
   		   var startCss = {top:0 - height}
           var endCss   = {top:0}
   		   elem.css(startCss).show().animate(endCss, 300)
   		   setTimeout(function () {
   			elem.animate(startCss, 500, function() {
   	            elem.hide()
   	        })
                }, 2000);
   		 */
       }
    </script>
    </body>
</html>