<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
        <title>现场报到</title>
        <link rel="stylesheet" href="/sdcenWX/Public/Home/css/weui.css"/>
        
        <style>
		.page, body {
		    background-color: #FBF9FE;
		}
		.weui_label {
		    display: block;
		    width: 4.7em;
		}
        </style>
    </head>
    <body>
       
        <div class="page" style="margin:80px auto;">
        
            <div class="hd">
                <h4  style="text-align:center;">欢迎您参加</h4>
                <h2 class="page_title" style="text-align:center;font-weight:bold;"><?php echo ($class["Name"]); ?></h2>
            </div>
        
            	<div class="bd" style="margin:40px auto 20px;">
            
					<form id="form">
						<input  type="hidden" name="openid" value="<?php echo ($openid); ?>"/>
						<input  type="hidden" name="ClassID" value="<?php echo ($ClassID); ?>"/>
		                <div class="weui_cells_title"style="text-align:center;">请输入您的身份证号确认报到</div>
		                <div class="weui_cells">
		                 <div class="weui_cell">
		                     <div class="weui_cell_hd"><label class="weui_label">身份证号:</label></div>
		                     <div class="weui_cell_bd weui_cell_primary">
		                         <input class="weui_input" type="text" name="CardNum" placeholder="请输入身份证号"/>
		                     </div>
		                 </div>
		                 </div>
		              </form>
	            </div>
	        		<div class="weui_btn_area">
                    <button class="weui_btn weui_btn_primary" id="submit">开始报到</button>
            		</div>
			
            <div class="weui_toptips weui_warn" id="error" style="dispalay:none;"><span id="errMsg"></span></div>
     </div>
        
    <script type='text/javascript' src='//g.alicdn.com/sj/lib/zepto/zepto.min.js' charset='utf-8'></script>
    <script>
     $(function(){
    	 
    	 $("#submit").click(function(){
			    if(checkParams()){
			    	var url = "/sdcenWX/index.php/Home/CheckIn/info";
			    	$("#form").attr("action", url); 
			    	$("#form").attr("method", "POST");
			    	$("#form").submit();
			    }		 
    	 });
    	 
     });
       function checkParams(){
    	   var idReg = /(^\d{15}$)|(^\d{17}([0-9]|X)$)/
   		   if(!idReg.test($("input[name=CardNum]").val())){
   			errShow("请填写正确的身份证号");
       		   return false;
   		   }
    	   return true;
       }
       
        function errShow(msg){
    	   var elem = $("#error");
    	   $("#errMsg").text(msg);
    	   elem.show();
    	   setTimeout(function(){
    		   elem.hide();
    	   },2000);
   		
       } 
    </script>
    </body>
</html>